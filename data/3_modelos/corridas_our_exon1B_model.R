
library("stringr")
library("reshape2")
library("parallel")

load("our/45_exon_dataset_skin-eye_malignant_melanoma_best_100.Rdata")
load("importancia_skin-melanoma_our.Rdata")
cantidad_variables = NULL
grupo = "our"
cantidad_best = 100
cancer = "skin-eye_malignant_melanoma"
tipo = "exon"

metricas_new_models <- function(sum_new_models, importancia){	
	data.frame(ADJR2=sapply(sum_new_models, FUN=function(X){X$adj.r.squared}), 
		R2=sapply(sum_new_models, FUN=function(X){X$r.squared}), 
		RSE=sapply(sum_new_models, FUN=function(X){X$sigma}), 
		sum_importancia=sapply(sum_new_models, FUN=function(X){sum(importancia[importancia$variable %in% rownames(X$coefficients)[-1], "importancia"])}))		
}

select_best_models <- function(sum_new_models, cantidad_best){
	numero_modelos <- length(sum_new_models[,1])
	
	if(numero_modelos > cantidad_best){
		sum_new_models$oADJR2 = NA
		sum_new_models$oR2 = NA		
		sum_new_models$oRSE = NA
		sum_new_models$osum_importancia = NA

		pos <- 1:numero_modelos

		#ordeno por las metricas
		k <- sort(sum_new_models$ADJR2, index.return = TRUE, decreasing = TRUE, na.last=TRUE)	
		sum_new_models[k$ix, "oADJR2"] <- pos	
		k <- sort(sum_new_models$R2, index.return = TRUE, decreasing = TRUE, na.last=TRUE)	
		sum_new_models[k$ix, "oR2"] <- pos	
		k <- sort(sum_new_models$RSE, index.return = TRUE, decreasing = FALSE, na.last=TRUE)	
		sum_new_models[k$ix, "oRSE"] <- pos	
		k <- sort(sum_new_models$sum_importancia, index.return = TRUE, decreasing = TRUE, na.last=TRUE)	
		sum_new_models[k$ix, "osum_importancia"] <- pos	

		#se suman las posiciones de cada modelo en el ordenamiento
		sum_model <- apply(sum_new_models[, c("oADJR2", "oR2", "oRSE", "osum_importancia")], 1, FUN=sum)
		sum_model <- sort(sum_model, index.return = TRUE, decreasing = FALSE, na.last=TRUE)	
		#recalculo su orden el que sea el primer modelo siempre tendrá el menor valor de suma
		#el que siempre sea el último tendrá el mayor valor en la suma 
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "ADJR2"] <- NA
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "R2"] <- NA
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "RSE"] <- NA	
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "sum_importancia"] <- NA	

	}
	
	sum_new_models[,c("ADJR2", "R2", "RSE", "sum_importancia")] 
}

variables_ya_models <- function(model, lista_modelos_siguiente, cores){
	if(length(lista_modelos_siguiente) > 0){
		t <- names(model$coefficients)[-1]
		cl <- makeCluster(8)
		value <- unique(unlist(parLapply(cl, lista_modelos_siguiente, fun=function(X,t){
			k <- setdiff(X, t)	
			if(length(k) == 1){
				k
			}else{ NULL }	
		}, t)))
		stopCluster(cl)	
		value
	}else{
		c()
	}
}


#calcular metricas
metricas <- function(sum_new_models, model){	
	c(RSE=sum_new_models$sigma, ADJR2 = sum_new_models$adj.r.squared, R2 = sum_new_models$r.squared)
}


cantidad_pasos <- min(length(can) - 1, length(can[,1]) - 1)
cantidad_pasos <- ifelse(!is.null(cantidad_variables) && as.integer(cantidad_variables) > 0, min(as.integer(cantidad_variables), cantidad_pasos), cantidad_pasos)	 
print(paste0("cancer type ", cancer, " maximo de pasos ", cantidad_pasos, " cantidad variables ", length(can) - 1 ," cantidad casos ", length(can[,1])))

#ordenar las variables
variables <- colnames(can)[-1]
variables <- factor(variables, levels=variables, ordered=TRUE) 

#modelos de cada paso
model_ini <- lm(total~1, can)
lista_modelos_paso <-list("1")
lista_modelos_siguiente <- list() 
#modelos analizados
analizados_modelos <- list() 
#na de cada paso
lista_na_paso <- list(c())	
lista_na_siguiente <- list() 


metricas_model <- data.frame() #dataframe con todas las metricas de los modelos seleccionados

#metricas de los modelos siguientes
best_paso_siguiente <- as.list(metricas(summary(model_ini), model_ini))

best_paso_siguiente2 <- list()

dataframe_metricas <- data.frame()

paso <- 0
model <- lm(paste0("total ~ ", paste0(lista_modelos_paso[[1]], collapse=" + ")), can)

while(paso < cantidad_pasos && length(lista_modelos_paso) > 0){
	print(paste0("paso: ", paso, " modelos: ",length(lista_modelos_paso)))
	while(length(lista_modelos_paso) > 0){
		#obtener el modelo 
		model <- lm(paste0("total ~ ", paste0(lista_modelos_paso[[1]], collapse=" + ")), can)			
		lista_modelos_paso <- lista_modelos_paso[-1]
		
		na_model_paso <- lista_na_paso[[1]] #indices de variables que dieron NA
		lista_na_paso <- lista_na_paso[-1]

		#genero los modelos a partir del modelo anterior sin contar con las variables NA
		h <- TRUE
		if(length(model$coefficients) > 1){ #significa el intercepto y al menos otra variable
			#marco las variables NA con FALSE			
			h <- !(variables %in% variables[na_model_paso]) & !(variables %in% names(model$coefficients[-1])) 
			#marco las variables donde ya se generó un modelo con FALSE
			h2 <- variables_ya_models(model, lista_modelos_siguiente, cores)
			if(length(h2) > 0){
				h <- h & !(variables %in% h2)
			}
		}
		#si tengo datos a adicionar
		if(any(h == TRUE)){
			#obtengo los nuevos modelos con esas variables
			hh <- c()			
			cl <- makeCluster(8)
			new_models <- parLapply(cl, as.list(levels(variables)[h]), fun=function(X_var, model, can){
				summary(update(model,  paste0("~ . + ",X_var)))		
			}, model, can)
			stopCluster(cl)	
			#seleccionar solo los modelos sin NA (Y$aliased), ya que Y$coefficients solo tiene las variables finales 
			#y con significación menor que 0.1 (sin comparar el intercepto), la primera fila de Y$coefficients es el intercepto
			cl <- makeCluster(8)			
			hh <- unlist(parLapply(cl, new_models, fun=function(Y){ any(Y$aliased == TRUE) || any(Y$coefficients[-1,"Pr(>|t|)"] > 0.1)}))	
			#ver si utilizar 0.05 ó 0.01
			stopCluster(cl)	
			gc()
		
			new_models[hh] <- NULL
			gc()

			#agregué los id de los na del padre a cada nuevo modelo generado
			na_new_models <- list(c(na_model_paso, c(1:length(variables))[h][hh]))
			na_new_models <- rep(na_new_models, length(new_models))
			#si quedaron modelos que son aptos, calcularle las métricas
			if(length(new_models) > 0){
				#las métricas agregarlas al data frame para el próximo paso			
				dataframe_metricas <- rbind(dataframe_metricas, metricas_new_models(new_models, importancia))
				#los nuevos modelos agregarlos a la lista del próximo paso al igual que con los NA
				cl <- makeCluster(8)
				new_models <- parLapply(cl, new_models, fun=function(X){rownames(X$coefficients)[-1]})
				stopCluster(cl)	
				gc()
			
				lista_modelos_siguiente <- c(lista_modelos_siguiente, new_models)
				lista_na_siguiente <- c(lista_na_siguiente, na_new_models)
				gc()

				# y de la nueva lista me quedo con los mejores
				#agregar los mejores para que sean menos modelos a verificar después	
				#tiene las métricas de cada modelo y NA los que no se vana a dejar
				#eliminar los modelos que no pasan en ningun métrica (todas las metricas NA)

				dataframe_metricas <- select_best_models(dataframe_metricas, cantidad_best) 
				k <- apply(dataframe_metricas, 1, FUN=function(X){all(is.na(X))})
				if(any(k == TRUE)){
					lista_modelos_siguiente[k] <- NULL
					lista_na_siguiente[k] <- NULL
					dataframe_metricas <- dataframe_metricas[!k,]		
				}
			}
		}
		gc()
	}
	if(length(lista_modelos_siguiente) > 0){
		#best_paso_siguiente2 tiene el mejor para el siguiente paso
		best_paso_siguiente2$ADJR2 <- max(best_paso_siguiente$ADJR2, dataframe_metricas$ADJR2, na.rm=TRUE)
		best_paso_siguiente2$R2 <- max(best_paso_siguiente$R2, dataframe_metricas$R2, na.rm=TRUE)
		best_paso_siguiente2$RSE <- min(best_paso_siguiente$RSE, dataframe_metricas$RSE, na.rm=TRUE)

		#seleccionar los modelos mejores que el padre...
		dataframe_metricas2 <- data.frame(ADJR2 = dataframe_metricas$ADJR2 >= best_paso_siguiente$ADJR2,
		R2 = dataframe_metricas$R2 >= best_paso_siguiente$R2,
		RSE = dataframe_metricas$RSE <= best_paso_siguiente$RSE, stringsAsFactors=FALSE)

		k <- apply(dataframe_metricas2, 1, FUN=function(X){any(X == TRUE)})
	
		#reescribir las variables y dejar los modelos que al menos se explora en alguna métrica
		lista_modelos_paso <- lista_modelos_siguiente[k]
		lista_na_paso <- lista_na_siguiente[k]
		metricas_model <- rbind(metricas_model, dataframe_metricas[k,])
		best_paso_siguiente <- best_paso_siguiente2
	
		#inicializar las cosas como vacío
		lista_modelos_siguiente <- list() 
		lista_na_siguiente <- list() 
		dataframe_metricas <- data.frame()
		best_paso_siguiente2 <- list()

		#agregarlo a la lista de modelos explorados
		analizados_modelos <- c(analizados_modelos, lista_modelos_paso)
		gc()
	}
	gc()
	paso <- paso + 1
}
print("saving models")
save(analizados_modelos, file=(paste0(ifelse(is.na(grupo),"our", grupo),"/45_", tipo,"_leapForward_", cancer, ifelse(is.na(grupo),"_best_", paste0("_",grupo, "_")), cantidad_best, ".Rdata")))

save(metricas_model, file=(paste0(ifelse(is.na(grupo),"our", grupo),"/45_", tipo,"_metricas_", cancer, ifelse(is.na(grupo),"_best_", paste0("_",grupo, "_")), cantidad_best, ".Rdata")))

rm(analizados_modelos, can, cantidad_pasos, lista_modelos_paso, lista_modelos_siguiente, lista_na_paso, lista_na_siguiente,  variables, metricas_model)
gc()

q(save="no")
