load("45_exon_leapForward_skin-eye_malignant_melanoma_oursin_100.Rdata")
load("45_exon_dataset_skin-eye_malignant_melanoma_oursin_100.Rdata")
#load("45_exon_leapForward_large_intestine_oursin_100.Rdata")
#load("45_exon_dataset_large_intestine_oursin_100.Rdata")
t <- lapply(analizados_modelos, FUN=function(X){paste0("total ~ ", paste0(X, collapse=" + "))})
k <- list()
for(X in t){
	k <- c(k, list(list(llamada=X, coeficientes=lm(X, can)$coefficients)))

}
analizados_modelos <- k
save(analizados_modelos, file="45_exon_leapForward_skin-eye_malignant_melanoma_oursin_100.Rdata")

#k <- lapply(t, FUN=function(X, can){list(llamada=X, coeficientes=lm(X, can)$coefficients)}, can)
#

