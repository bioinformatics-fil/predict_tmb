#forward_mio_all_models(cancer_begin_id=32,cancer_end_id=42, cantidad_best=100, cantidad_variables = NULL, tipo="exon")

library("stringr")


forward_mio_all_models <- function(cancer_begin_id=1, cancer_end_id=1, cantidad_variables = NULL, cantidad_best=1, tipo=NA, grupo=NA, cores=1){
#tipo NA, "gene", "exon"
#grupo NA (significa nuestra seleccion), "census", "panel", "oursin" (sin los 11 genes más largos)
print("iniciando estudio")	
#cancers
cancer <- c("adrenal gland tumor", "neuroblastoma", "biliary tract carcinoma", "chondro and osteosarcoma", "breast carcinoma", "cervical carcinoma", "glioma", "medulloblastoma", "endometrial carcinoma", "germ cell tumor", "myeloid leukaemia", "clear cell renal carcinoma", "papillary cell renal carcinoma", "renal cell carcinoma", "wilms tumor", "colorectal carcinoma", "liver carcinoma", "non-small cell lung carcinoma", "small cell lung carcinoma", "lymphoid leukaemia", "meningioma", "esophageal adenocarcinoma", "esophageal other tumor", "esophageal squamous carcinoma", "ovarian other carcinoma", "ovarian serous carcinoma", "pancreatic endocrine tumor", "pancreatic tumor", "parathyroid tumor", "pituitary tumor", "mesothelioma", "prostate carcinoma", "salivary gland tumor", "non-melanoma skin tumor", "melanoma", "small intestine tumor", "soft tissue sarcoma", "gastic carcinoma", "thyroid carcinoma", "head and neck tumor", "uninary tract other", "urinary tract transitional cell")


load("10_whole_cosmic_fix_new.Rdata")
#carga mutacional total en cada sample
cosa <- unique(whole_cosmic_fixed[, c("ID_sample", "ID_coordenada")])
cosa <- aggregate(cosa$ID_coordenada, by=list(ID_sample = cosa$ID_sample), FUN=function(X){length(X)})
colnames(cosa)[2] <- "total"
rm(whole_cosmic_fixed)
gc()

#entrenamiento
library("reshape2")
if(cores > 1){
library("parallel")
}

if(is.na(tipo) || (tipo == "gene")){	
	print("select genes")
	if(is.na(grupo)){
		load("43_subset_gene_clean_select.Rdata")
	}else if(grupo == "oursel"){
		load("43_subset_gene_clean_select_sinsel.Rdata")
	}else if(grupo == "oursel99"){
		load("43_subset_gene_clean_select_sinsel99.Rdata")
	}else if(grupo == "oursin"){
		load("43_subset_gene_clean_select_sin.Rdata")
	}else if(grupo == "census"){
		load("43_subset_gene_census.Rdata")
	}else if (grupo == "panel"){
		load("43_subset_gene_panel.Rdata")
	}
	#datos de select
	select <- unique(whole_cosmic_select[, c("cancer_type", "ID_sample", "gene_ensembl", "ID_coordenada")])
	importancia <- unique(whole_cosmic_select[, c("cancer_type", "gene_ensembl", "importancia")])
	rm(whole_cosmic_select)
	gc()	
	colnames(importancia)[2] <- "variable"
	importancia$variable <- as.character(importancia$variable)
	importancia <- importancia[order(importancia$cancer_type, -importancia$importancia), ]
	
	for(cancer_id in cancer_begin_id:cancer_end_id){ 
		print(paste0("genes del ", cancer[cancer_id]))
		forward_inter(tipo = "gene", cancer=cancer[cancer_id], cosa=cosa, cantidad_variables = cantidad_variables , select = select[select$cancer_type %in% cancer[cancer_id],], cantidad_best=cantidad_best,grupo=grupo, importancia=importancia[importancia$cancer_type %in% cancer[cancer_id],], cores) 

	}
}
if(is.na(tipo) || (tipo == "exon")){
	print("select exones")
	if(is.na(grupo)){
		load("43_subset_exon_clean_select.Rdata")
	}else if(grupo == "oursel"){
		load("43_subset_exon_clean_select_sinsel.Rdata")
	}else if(grupo == "oursel99"){
		load("43_subset_exon_clean_select_sinsel99.Rdata")
	}else if(grupo == "oursin"){
		load("43_subset_exon_clean_select_sin.Rdata")
	}else if(grupo == "census"){
		load("43_subset_exon_census.Rdata")
	}else if (grupo == "panel"){
		load("43_subset_exon_panel.Rdata")
	}
	#datos de select
	select <- unique(whole_cosmic_select[, c("cancer_type", "ID_sample", "exon_ensembl", "ID_coordenada")])
	importancia <- unique(whole_cosmic_select[, c("cancer_type", "exon_ensembl", "importancia")])
	rm(whole_cosmic_select)
	gc()
	colnames(importancia)[2] <- "variable"
	importancia$variable <- as.character(importancia$variable)
	importancia <- importancia[order(importancia$cancer_type, -importancia$importancia), ]

	for(cancer_id in cancer_begin_id:cancer_end_id ){ 
		print(paste0("exones del ", cancer[cancer_id]))
		forward_inter(tipo= "exon", cancer=cancer[cancer_id], cosa=cosa, cantidad_variables = cantidad_variables , select = select[select$cancer_type %in% cancer[cancer_id],], cantidad_best=cantidad_best, grupo=grupo, importancia=importancia[importancia$cancer_type %in% cancer[cancer_id],], cores)
	}
}
}

#mejorado
forward_inter <- function(tipo=c("gene", "exon"), cosa, cancer, cantidad_variables, select, cantidad_best, grupo=c(NA,"census", "panel", "oursin"), importancia, cores){

can <- select[, c("ID_sample", ifelse(tipo == "gene", "gene_ensembl", "exon_ensembl"), "ID_coordenada")]
#pone en cada fila las samples y en las columnas los genes y el valor es la cantidad de ID_coordenada
rm(select)
gc()
	
if(tipo == "gene"){
	can <- dcast(can, ID_sample~gene_ensembl, fun.aggregate=length, fill=0)
}else{
	can <- dcast(can, ID_sample~exon_ensembl, fun.aggregate=length, fill=0)
}
#agregar a can el número de carga total.
can <- merge(cosa, can)[2:(length(can) + 1)]
can <- can[, c("total", importancia$variable)]
cancer_print <- str_replace(cancer, " ", "_")	

save(can, file=(paste0(ifelse(is.na(grupo),"our", grupo),"/45_", tipo,"_dataset_", cancer_print, ifelse(is.na(grupo),"_best_", paste0("_",grupo, "_")), cantidad_best, ".Rdata")))

cantidad_pasos <- min(length(can) - 1, length(can[,1]) - 1)
cantidad_pasos <- ifelse(!is.null(cantidad_variables) && as.integer(cantidad_variables) > 0, min(as.integer(cantidad_variables), cantidad_pasos), cantidad_pasos)	 
print(paste0("cancer type ", cancer_print, " maximo de pasos ", cantidad_pasos, " cantidad variables ", length(can) - 1 ," cantidad casos ", length(can[,1])))

#ordenar las variables
variables <- colnames(can)[-1]
variables <- factor(variables, levels=variables, ordered=TRUE) 

#modelos de cada paso
model_ini <- lm(total~1, can)
lista_modelos_paso <-list(model_ini )
lista_modelos_siguiente <- list() 
#modelos analizados
analizados_modelos <- list() 
#na de cada paso
lista_na_paso <- list(c())	
lista_na_siguiente <- list() 


metricas_model <- data.frame() #dataframe con todas las metricas de los modelos seleccionados

#metricas de los modelos siguientes
best_paso_siguiente <- as.list(metricas(summary(model_ini), model_ini))

best_paso_siguiente2 <- list()

dataframe_metricas <- data.frame()

paso <- 0
while(paso < cantidad_pasos && length(lista_modelos_paso) > 0){
	print(paste0("paso: ", paso, " modelos: ",length(lista_modelos_paso)))
	while(length(lista_modelos_paso) > 0){
		#obtener el modelo 
		model <- lista_modelos_paso[[1]]			
		lista_modelos_paso <- lista_modelos_paso[-1]
		
		na_model_paso <- lista_na_paso[[1]] #indices de variables que dieron NA
		lista_na_paso <- lista_na_paso[-1]

		#genero los modelos a partir del modelo anterior sin contar con las variables NA
		h <- TRUE
		if(length(model$coefficients) > 1){ #significa el intercepto y al menos otra variable
			#marco las variables NA con FALSE			
			h <- !(variables %in% variables[na_model_paso]) & !(variables %in% names(model$coefficients[-1])) 
			#marco las variables donde ya se generó un modelo con FALSE
			h2 <- variables_ya_models(model, lista_modelos_siguiente, cores)
			if(length(h2) > 0){
				h <- h & !(variables %in% h2)
			}
		}
		#si tengo datos a adicionar
		if(any(h == TRUE)){
			#obtengo los nuevos modelos con esas variables
			new_models <-list()
			sum_new_models <-list()
			hh <- c()
			if(cores == 1){
			new_models <- lapply(as.list(levels(variables)[h]), FUN=function(X_var){
				update(model,  paste0("~ . + ",X_var))		
			}, model)
			#obtener los datos de cada modelos 
			sum_new_models <- lapply(new_models, FUN=summary)
			
			#seleccionar solo los modelos sin NA (Y$aliased), ya que Y$coefficients solo tiene las variables finales 
			#y con significación menor que 0.1 (sin comparar el intercepto), la primera fila de Y$coefficients es el intercepto
			hh <- unlist(lapply(sum_new_models, FUN=function(Y){ any(Y$aliased == TRUE) || any(Y$coefficients[-1,"Pr(>|t|)"] > 0.1)}))	#ver si utilizar 0.05 ó 0.01		
			}else{
			cl <- makeCluster(2, type="FORK")
			new_models <- parLapply(cl,as.list(levels(variables)[h]), fun=function(X_var){
				update(model,  paste0("~ . + ",X_var))		
			})
			#obtener los datos de cada modelos 
			sum_new_models <- parLapply(cl, new_models, fun=summary)
			
			#seleccionar solo los modelos sin NA (Y$aliased), ya que Y$coefficients solo tiene las variables finales 
			#y con significación menor que 0.1 (sin comparar el intercepto), la primera fila de Y$coefficients es el intercepto
			hh <- unlist(parLapply(cl, sum_new_models, fun=function(Y){ any(Y$aliased == TRUE) || any(Y$coefficients[-1,"Pr(>|t|)"] > 0.1)}))	
			#ver si utilizar 0.05 ó 0.01
			stopCluster(cl)	
			gc()
			}
		
			new_models[hh] <- NULL
			sum_new_models[hh] <- NULL

			#agregué los id de los na del padre a cada nuevo modelo generado
			na_new_models <- list(c(na_model_paso, c(1:length(variables))[h][hh]))
			na_new_models <- rep(na_new_models, length(new_models))
			#si quedaron modelos que son aptos, calcularle las métricas
			if(length(new_models) > 0){			
							
				sum_new_models <- metricas_new_models(sum_new_models, importancia, cores)
				#calcularle todas las metricas, agregar una metrica para importancia donde se sume la importancia de los genes involucrados
				
				#los nuevos modelos agregarlos a la lista del próximo paso al igual que con los NA
				lista_modelos_siguiente <- c(lista_modelos_siguiente, new_models)
				lista_na_siguiente <- c(lista_na_siguiente, na_new_models)
				#las métricas agregarlas al data frame para el próximo paso
				dataframe_metricas <- rbind(dataframe_metricas, sum_new_models)	

				# y de la nueva lista me quedo con los mejores
				#agregar los mejores para que sean menos modelos a verificar después	
				#tiene las métricas de cada modelo y NA los que no se vana a dejar
				#eliminar los modelos que no pasan en ningun métrica (todas las metricas NA)

				dataframe_metricas <- select_best_models(dataframe_metricas, cantidad_best) 
				k <- apply(dataframe_metricas, 1, FUN=function(X){all(is.na(X))})
				if(any(k == TRUE)){
					lista_modelos_siguiente[k] <- NULL
					lista_na_siguiente[k] <- NULL
					dataframe_metricas <- dataframe_metricas[!k,]		
				}
			}
		}
		gc()
	}
	if(length(lista_modelos_siguiente) > 0){
		#best_paso_siguiente2 tiene el mejor para el siguiente paso
		best_paso_siguiente2$ADJR2 <- max(best_paso_siguiente$ADJR2, dataframe_metricas$ADJR2, na.rm=TRUE)
		best_paso_siguiente2$R2 <- max(best_paso_siguiente$R2, dataframe_metricas$R2, na.rm=TRUE)
		best_paso_siguiente2$RSE <- min(best_paso_siguiente$RSE, dataframe_metricas$RSE, na.rm=TRUE)

		#seleccionar los modelos mejores que el padre...
		dataframe_metricas2 <- data.frame(ADJR2 = dataframe_metricas$ADJR2 >= best_paso_siguiente$ADJR2,
		R2 = dataframe_metricas$R2 >= best_paso_siguiente$R2,
		RSE = dataframe_metricas$RSE <= best_paso_siguiente$RSE, stringsAsFactors=FALSE)

		k <- apply(dataframe_metricas2, 1, FUN=function(X){any(X == TRUE)})
	
		#reescribir las variables y dejar los modelos que al menos se explora en alguna métrica
		lista_modelos_paso <- lista_modelos_siguiente[k]
		lista_na_paso <- lista_na_siguiente[k]
		metricas_model <- rbind(metricas_model, dataframe_metricas[k,])
		best_paso_siguiente <- best_paso_siguiente2
	
		#inicializar las cosas como vacío
		lista_modelos_siguiente <- list() 
		lista_na_siguiente <- list() 
		dataframe_metricas <- data.frame()
		best_paso_siguiente2 <- list()

		#agregarlo a la lista de modelos explorados
		if(cores == 1){
		analizados_modelos <- c(analizados_modelos, lapply(lista_modelos_paso, FUN=function(X){list(coeficientes = X$coefficients, llamada = gsub("\\n\\s+", "", as.character(X$call)[2], perl=TRUE))}))
		}else{
		cl <- makeCluster(2)
		analizados_modelos <- c(analizados_modelos, parLapply(cl, lista_modelos_paso, fun=function(X){list(coeficientes = X$coefficients, llamada = gsub("\\n\\s+", "", as.character(X$call)[2], perl=TRUE))}))
		stopCluster(cl)
		gc()
		}
	}
	gc()
	paso <- paso + 1
}
print("saving models")
save(analizados_modelos, file=(paste0(ifelse(is.na(grupo),"our", grupo),"/45_", tipo,"_leapForward_", cancer_print, ifelse(is.na(grupo),"_best_", paste0("_",grupo, "_")), cantidad_best, ".Rdata")))

save(metricas_model, file=(paste0(ifelse(is.na(grupo),"our", grupo),"/45_", tipo,"_metricas_", cancer_print, ifelse(is.na(grupo),"_best_", paste0("_",grupo, "_")), cantidad_best, ".Rdata")))

rm(analizados_modelos, can, cantidad_pasos, lista_modelos_paso, lista_modelos_siguiente, lista_na_paso, lista_na_siguiente,  variables, metricas_model)
gc()
cancer
}


metricas_new_models <- function(sum_new_models, importancia, cores){	
	data.frame(ADJR2=sapply(sum_new_models, FUN=function(X){X$adj.r.squared}), 
		R2=sapply(sum_new_models, FUN=function(X){X$r.squared}), 
		RSE=sapply(sum_new_models, FUN=function(X){X$sigma}), 
		sum_importancia=sapply(sum_new_models, FUN=function(X){sum(importancia[importancia$variable %in% rownames(X$coefficients)[-1], "importancia"])}))		
}

select_best_models <- function(sum_new_models, cantidad_best){
	numero_modelos <- length(sum_new_models[,1])
	
	if(numero_modelos > cantidad_best){
		sum_new_models$oADJR2 = NA
		sum_new_models$oR2 = NA		
		sum_new_models$oRSE = NA
		sum_new_models$osum_importancia = NA

		pos <- 1:numero_modelos

		#ordeno por las metricas
		k <- sort(sum_new_models$ADJR2, index.return = TRUE, decreasing = TRUE, na.last=TRUE)	
		sum_new_models[k$ix, "oADJR2"] <- pos	
		k <- sort(sum_new_models$R2, index.return = TRUE, decreasing = TRUE, na.last=TRUE)	
		sum_new_models[k$ix, "oR2"] <- pos	
		k <- sort(sum_new_models$RSE, index.return = TRUE, decreasing = FALSE, na.last=TRUE)	
		sum_new_models[k$ix, "oRSE"] <- pos	
		k <- sort(sum_new_models$sum_importancia, index.return = TRUE, decreasing = TRUE, na.last=TRUE)	
		sum_new_models[k$ix, "osum_importancia"] <- pos	

		#se suman las posiciones de cada modelo en el ordenamiento
		sum_model <- apply(sum_new_models[, c("oADJR2", "oR2", "oRSE", "osum_importancia")], 1, FUN=sum)
		sum_model <- sort(sum_model, index.return = TRUE, decreasing = FALSE, na.last=TRUE)	
		#recalculo su orden el que sea el primer modelo siempre tendrá el menor valor de suma
		#el que siempre sea el último tendrá el mayor valor en la suma 
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "ADJR2"] <- NA
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "R2"] <- NA
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "RSE"] <- NA	
		sum_new_models[sum_model$ix[(cantidad_best + 1): numero_modelos], "sum_importancia"] <- NA	

	}
	
	sum_new_models[,c("ADJR2", "R2", "RSE", "sum_importancia")] 
}

variables_ya_models <- function(model, lista_modelos_siguiente, cores){
	if(length(lista_modelos_siguiente) > 0){
		t <- names(model$coefficients)[-1]
		if(cores==1){
		unique(unlist(sapply(lista_modelos_siguiente, FUN=function(X){
			k <- setdiff(names(X$coefficients)[-1], t)	
			if(length(k) == 1){
				k
			}else{ NULL }	
		})))	
		}else{
		cl <- makeCluster(2, type="FORK")
		unique(unlist(parLapply(cl, lista_modelos_siguiente, fun=function(X){
			k <- setdiff(names(X$coefficients)[-1], t)	
			if(length(k) == 1){
				k
			}else{ NULL }	
		}, t)))
		stopCluster(cl)	
		gc()
		}		
	}else{
		c()
	}
}


#calcular metricas
metricas <- function(sum_new_models, model){	
	c(RSE=sum_new_models$sigma, ADJR2 = sum_new_models$adj.r.squared, R2 = sum_new_models$r.squared)
}

select_models <- function(file=".", var=NULL){
percent_sel=75
#file "our" "census", "panel"
#data puede ser el data guardado previamente
#seleccion es el % a partir del cual seleccionar.
library("stringr")
load("40_statistics_cancer_type.Rdata")

lista_archivos <- list.files(file, "leapForward")
if(!is.null(var)){
lista_archivos <- lista_archivos[var]
}
for(i in lista_archivos){
	print(i)
	#cargar metricas y modelos	
	load(paste0(file, "/", i))	
	load(paste0(file, "/", str_replace(i, pattern="leapForward", "metricas")[[1]]))
	load(paste0(file, "/", str_replace(i, pattern="leapForward", "dataset")[[1]]))
	if(dim(metricas_model)[1] > 0){
	k <- str_split(i, "_")[[1]]
	tipo <- k[2]
	cancer <- paste0(k[c(4:(length(k) - 2))], collapse=" ") 
	cancer_print <- str_replace(cancer, " ", "_")	
	selection <- k[length(k) - 1]
	if(selection == "best"){
		selection <- "our"
	}
	#calculares todas las métricas
	metricas_model <- cbind(data.frame(cancer_type = cancer, tipo = tipo, selection=selection,  total_samples = length(can[,1]), modelo = sapply(analizados_modelos, FUN=function(X){X$llamada}), variables_model = sapply(analizados_modelos, FUN=function(X){length(X$coeficientes) - 1}), variables_negativas = sapply(analizados_modelos, FUN=function(X){y <- X$coeficientes[-1]; length(y[ y < 0])}), samples_represented = sapply(analizados_modelos, FUN=function(X){k <- apply(can[, c("total",names(X$coeficientes[-1]))],1,FUN=function(X){sum(X[-1])});length(k[k>0])}), stringsAsFactors=FALSE), metricas_model)
	
	#esto se guarda una sola vez para 75 y 95 percent, pues son los mismos datos
	save(metricas_model, file=paste0("sel_", selection, "/46_allmetric_",tipo, "_", selection, "_", cancer_print , "_sinpb.Rdata"))

	x <- max(0.6, min(0.8, as.numeric(quantile(metricas_model$R2, percent_sel/100.0))))
	#el RSE menor que la mediana mutacional
	y <- min(statistic_cancer_type[statistic_cancer_type$cancer_type %in% cancer, "median"], as.numeric(quantile(metricas_model$RSE, (1-(percent_sel/100.0)))))
	z <- min(floor(as.numeric(quantile(metricas_model$samples_represented, percent_sel/100.0))), floor(0.8*length(can[,1])))

	#seleccionar los modelos con R2 >= 0.6 como posibles mejores
	#además que represente a la mayor cantidad de samples posible
	
	print(paste0("select models R2 exones [", x, " , ", max(metricas_model$R2), "]"))
	print(paste0("RSE [", min(metricas_model$RSE), " , ", y, "]"))
	print(paste0("represented samples [", z, " , ", max(metricas_model$samples_represented),"] (", length(can[,1]), " total-samples)"))
	 #modelos seleccionados
	h <- metricas_model$R2 >= x & metricas_model$RSE <= y & metricas_model$samples_represented >= z
	select_models <- analizados_modelos[h]	
	metricas_selmodel <- metricas_model[h, ]

	rm(analizados_modelos, can, metricas_model)
	gc()

	print(paste0("selecting ",length(metricas_selmodel$modelo)," models"))
	if(length(metricas_selmodel$modelo) > 0){
		save(select_models, file=paste0("sel_", selection, "/46_selmodels_",tipo, "_", selection, "_", cancer_print , "_sinpb.Rdata"))	
		save(metricas_selmodel, file=paste0("sel_", selection, "/46_selmetric_",tipo, "_", selection, "_", cancer_print , "_sinpb.Rdata"))	
	}
	gc()
	}
}
}



train_select_models <- function(file="", var=NULL){ 
#file es lac arpeta sel_bbbbb
library("caret")
library("stringr")

lista_archivos <- list.files(file, "selmetric")
if(!is.null(var)){
lista_archivos <-lista_archivos[var]
}
col_sel <- c("Rsquared_OptBoot", "RMSE_OptBoot", "MAE_OptBoot")
col_names <- c("Rsquared_1000optboot", "RMSE_1000optboot", "MAE_1000optboot")
train.control <- trainControl(method = "boot_all", number = 1000)
for(i in lista_archivos){
	print(i)
	#obtener datos del nombre del file
	k <- str_split(i, "_")[[1]]
	tipo <- k[3]
	selection <- k[4]
	cancer <- paste0(k[c(5:(length(k) - 1))], collapse="_") 
	cancer_print <- str_replace(cancer, " ", "_")	
			
	#levantar modelos select, statistics y dataset	
	load(paste0(file, "/", i))
	load(paste0(selection, "/45_", tipo, "_dataset_", cancer_print, "_" ,ifelse(selection == "our", "best", selection), "_100.Rdata"))

	print(paste0("trainning ",dim(metricas_selmodel)[1]," models"))
	if(dim(metricas_selmodel)[1] > 0){		
		#entrenar los seleccionados
		models_sel <- lapply(as.list(metricas_selmodel$modelo), FUN=function(Y, can){ 
			set.seed(19900531)	
			(train(formula(Y), data = can, method = "lm", trControl = train.control, metric="RMSE", maximize=TRUE))$results
		}, can)
		gc()
		pp <- Reduce(f=rbind, x=models_sel) 
		rm(models_sel)
		pp <- pp[, col_sel]
		colnames(pp) <- col_names
		metricas_selmodel <- cbind(metricas_selmodel, pp)
		#si no guardo los modelos entrenados eliminarlos
		rm(pp)
		save(metricas_selmodel, file=paste0(str_replace(file, "sel", "train"), "/", str_replace(i, "46_sel", "47_train")))	
	}
	rm(can)
	gc()	
	rm(metricas_selmodel)
	gc()

}
}

tamano_modelos_pb <- function(){
#	
load("10_whole_cosmic_fix_new.Rdata")
datos_all <- unique(whole_cosmic_fixed[, c("gene_ensembl", "transcript_ensembl", "exon_ensembl")])
rm(whole_cosmic_fixed)
gc()

library("GenomicFeatures")
txdb <- loadDb("5_txdb_database.Db")

#seleccionar los modelos tipo exon

lista_archivos <- list.files(".", "^46_sel_")
datos_s <- strsplit(lista_archivos, split="_")
datos_s <- lapply(datos_s, FUN=function(X){X[2] <- "sel2"; X})
datos_s <- sapply(datos_s, FUN=function(X){paste0(X, collapse="_")})
for(i in 1:length(lista_archivos)){
	print(i)
	load(lista_archivos[i])

	
	print("tamaño de modelos exones")
	h <- data_sel$tipo %in% "exon"
	#calcular el tamaño en pares de bases por cada modelo
	#poner a cada tipo de selección la cantidad de pares de bases a secuenciar
	exones <- strsplit(x=data_sel[h,"modelo"], split="\\s+", perl=TRUE)
	pos <- lapply(exones, FUN=function(X){ seq(from=3, to=length(X), by=2) })
	exones <- lapply(as.list(1:length(exones)), FUN=function(X){exones[[X]][ pos[[X]] ]})
	print(paste0("tamaño a ",length(exones) ," modelos"))
	data_sel[h, "length_pb"] <- sapply(exones, FUN=function(X){
		x_unique <- datos_all[datos_all$exon_ensembl %in% X, c("gene_ensembl", "transcript_ensembl", "exon_ensembl")]
		datos <- cds(txdb, columns=c("exon_name", "tx_name"), filter=list(exon_name=unique(x_unique$exon_ensembl)))
		#una mejor forma
		x_exon <- unlist(datos$exon_name)
		x_tx <- unlist(datos$tx_name)
		#puedo utilizar cualquiera de los campos (exon_name, exon_rank ó tx_name)
		solo_tx_name<- datos$tx_name
		count_repeat <- sapply(solo_tx_name, FUN=function(X){length(X)})
		x_chrom <- rep(as.character(seqnames(datos)), times=count_repeat)
		x_start <- rep(start(datos), times=count_repeat)
		x_end <- rep(end(datos), times=count_repeat)
		x_strand <- rep(as.character(strand(datos)), times=count_repeat)
		datos  <- data.frame(seqnames=x_chrom, start=x_start, end=x_end,  strand=x_strand, exon_name=x_exon, transcript_ensembl=x_tx)
		datos  <- merge(datos, x_unique)
		datos <- GRanges(datos)
		datos <- disjoin(datos)
		sum(width(datos))
	})
	
	print("tamaño de modelos genes")
	h <- data_sel$tipo %in% "gene"
	genes <- strsplit(x=data_sel[h,"modelo"], split="\\s+|,", perl=TRUE)
	pos <- lapply(genes, FUN=function(X){ seq(from=3, to=length(X), by=2) })
	genes <- lapply(as.list(1:length(genes)), FUN=function(X){genes[[X]][ pos[[X]] ]})
	print(paste0("tamaño a ",length(genes) ," modelos"))
	data_sel[h, "length_pb"] <- sapply(genes, FUN=function(X){
		x_unique <- unique(datos_all[datos_all$gene_ensembl %in% X , c("gene_ensembl", "transcript_ensembl")])  
		datos <- cds(txdb, columns=c("exon_name", "tx_name"), filter=list(tx_name=unique(x_unique$transcript_ensembl)))
		#una mejor forma
		x_exon <- unlist(datos$exon_name)
		x_tx <- unlist(datos$tx_name)
		#puedo utilizar cualquiera de los campos (exon_name, exon_rank ó tx_name)
		solo_tx_name<- datos$tx_name
		count_repeat <- sapply(solo_tx_name, FUN=function(X){length(X)})
		x_chrom <- rep(as.character(seqnames(datos)), times=count_repeat)
		x_start <- rep(start(datos), times=count_repeat)
		x_end <- rep(end(datos), times=count_repeat)
		x_strand <- rep(as.character(strand(datos)), times=count_repeat)
		datos  <- data.frame(seqnames=x_chrom, start=x_start, end=x_end,  strand=x_strand, exon_name=x_exon, transcript_ensembl=x_tx)
		datos  <- merge(datos, x_unique)
		datos <- GRanges(datos)
		datos <- disjoin(datos)
		sum(width(datos))
	})
save(data_sel, file=datos_s[i])
print(i)
}
}


statistic_data <- function(tipo=NA){
#tipo NA, "panel", "census"
cancer <- data.frame(cancer_type = c("adrenal gland tumor", "neuroblastoma", "biliary tract carcinoma", "chondro and osteosarcoma", "breast carcinoma", "cervical carcinoma", "glioma", "medulloblastoma", "endometrial carcinoma", "germ cell tumor", "myeloid leukaemia", "clear cell renal carcinoma", "papillary cell renal carcinoma", "renal cell carcinoma", "wilms tumor", "colorectal carcinoma", "liver carcinoma", "non-small cell lung carcinoma", "small cell lung carcinoma", "lymphoid leukaemia", "meningioma", "esophageal adenocarcinoma", "esophageal other tumor", "esophageal squamous carcinoma", "ovarian other carcinoma", "ovarian serous carcinoma", "pancreatic endocrine tumor", "pancreatic tumor", "parathyroid tumor", "pituitary tumor", "mesothelioma", "prostate carcinoma", "salivary gland tumor", "non-melanoma skin tumor", "melanoma", "small intestine tumor", "soft tissue sarcoma", "gastic carcinoma", "thyroid carcinoma", "head and neck tumor", "uninary tract other", "urinary tract transitional cell"), stringsAsFactors=FALSE)

lista_archivos <- list.files(".", "^46_all")

datos_s <- strsplit(lista_archivos, split="_")

lista_archivos <- data.frame(file_name_all = lista_archivos, file_name_sel = sapply(datos_s, FUN=function(X){X[2] <- "sel2"; paste0(X, collapse="_")}), cancer_type = sapply(datos_s, FUN=function(X){paste0(X[ifelse(is.na(tipo),3,4):(length(X)-1)], collapse=" ")}), stringsAsFactors=FALSE)

lista_archivos <- merge(cancer, lista_archivos, sort=FALSE)

statistic_data <- merge(cancer, data.frame(tipo=c("gene", "exon"), stringsAsFactors=FALSE))
statistic_data$modelos_total <- 0
statistic_data$modelos_sel <- 0
statistic_data$R2_3q <- 0
statistic_data$max_R2 <- 0

all_model_data <- data.frame()

all_model_sel_data <- data.frame()

for(i in 1:length(lista_archivos[,1])){
	load(lista_archivos[i, "file_name_all"])
	s <- aggregate(data_all$tipo, by=list(tipo=data_all$tipo), FUN=length)
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "exon" , "modelos_total"] <- s[s$tipo %in% "exon", "x"]
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "gene" , "modelos_total"] <- s[s$tipo %in% "gene", "x"]
	
	load(lista_archivos[i, "file_name_sel"])	
	s <- aggregate(data_sel$R2, by=list(tipo=data_sel$tipo), FUN=length)
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "exon" , "modelos_sel"] <- s[s$tipo %in% "exon", "x"]
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "gene" , "modelos_sel"] <- s[s$tipo %in% "gene", "x"]
	s <- aggregate(data_sel$R2, by=list(tipo=data_sel$tipo), FUN=min)
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "exon" , "R2_3q"] <- s[s$tipo %in% "exon", "x"]
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "gene" , "R2_3q"] <- s[s$tipo %in% "gene", "x"]
	s <- aggregate(data_sel$R2, by=list(tipo=data_sel$tipo), FUN=max)
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "exon" , "max_R2"] <- s[s$tipo %in% "exon", "x"]
	statistic_data[statistic_data$cancer_type %in% lista_archivos[i, "cancer_type"] & statistic_data$tipo %in% "gene" , "max_R2"] <- s[s$tipo %in% "gene", "x"]
	
	data_all$sel <- data_all$modelo %in% data_sel$modelo
	all_model_data <- rbind(all_model_data, data_all)

	all_model_sel_data <- rbind(all_model_sel_data, data_sel)

	rm(data_sel, data_all,s)
	gc()
	print(i)
}
statistic_data$modelos_percent <- statistic_data$modelos_sel/statistic_data$modelos_total
save(statistic_data, file=paste0("47_statistic",ifelse(is.na(tipo), "", paste0("_", tipo)),".Rdata"))
save(all_model_data, file=paste0("47_data_model_all",ifelse(is.na(tipo), "", paste0("_", tipo)),".Rdata"))
save(all_model_sel_data, file=paste0("47_datasel_model_all",ifelse(is.na(tipo), "", paste0("_", tipo)),".Rdata"))
}



compare_train <- function(){
	library("PMCMRplus")
	library("caret")
	table_model_all <- data.frame()
	#comparar resultados no pareados
	#kruskal wallis

cancer <- data.frame(cancer_type = c("adrenal gland tumor", "neuroblastoma", "biliary tract carcinoma", "chondro and osteosarcoma", "breast carcinoma", "cervical carcinoma", "glioma", "medulloblastoma", "endometrial carcinoma", "germ cell tumor", "myeloid leukaemia", "clear cell renal carcinoma", "papillary cell renal carcinoma", "renal cell carcinoma", "wilms tumor", "colorectal carcinoma", "liver carcinoma", "non-small cell lung carcinoma", "small cell lung carcinoma", "lymphoid leukaemia", "meningioma", "esophageal adenocarcinoma", "esophageal other tumor", "esophageal squamous carcinoma", "ovarian other carcinoma", "ovarian serous carcinoma", "pancreatic endocrine tumor", "pancreatic tumor", "parathyroid tumor", "pituitary tumor", "mesothelioma", "prostate carcinoma", "salivary gland tumor", "non-melanoma skin tumor", "melanoma", "small intestine tumor", "soft tissue sarcoma", "gastic carcinoma", "thyroid carcinoma", "head and neck tumor", "uninary tract other", "urinary tract transitional cell"), stringsAsFactors=FALSE)

lista_archivos <- list.files(".", "^46_modeltrain_")

datos_s <- strsplit(lista_archivos, split="_")
lista_archivos <- data.frame(cancer_type = sapply(datos_s, FUN=function(X){paste0(X[3:(length(X)-1)], collapse=" ")}), file=lista_archivos, stringsAsFactors=FALSE)
lista_archivos <- merge(cancer, lista_archivos)
cancers <- unique(cancer$cancer_type)

for(cancer in cancers){
	load(lista_archivos[(lista_archivos$cancer_type %in% cancer), "file"])
	names_m <- paste0("model_", 1:length(models_sel))
	table_m <- data.frame(model = names_m, average_RMSE = sapply(models_sel, FUN=function(X){X$results$RMSE}), stringsAsFactors = FALSE)
	table_m <- table_m[order(table_m$average_RMSE), ]
	best <- table_m[1, "model"]
	table <- data.frame(model = factor(rep(names_m, each=30), levels=table_m$model), RMSE = unlist(lapply(models_sel, FUN=function(X){X$resample$RMSE})))
	table <- kwManyOneConoverTest(RMSE~model, data=table, p.adjust.method = "holm")
	#no son iguales al mejor
	k <- names(table$p.value[table$p.value < 0.05,])
	#table_m poner cada cancer type
	table_m[table_m$model %in% k, "tipo" ] <- "diferente"
	table_m[is.na(table_m$tipo), "tipo" ] <- "igual"
	table_m[table_m$model %in% best, "tipo" ] <- "best"
	table_m$cancer_type <- cancer
	table_model_all <- rbind(table_model_all, table_m)
}

#diferente rojo, igual azul, best verde
table_model_all$tipo <- factor(table_model_all$tipo, levels=c("diferente", "igual", "best")) 
save(table_model_all, file="47_table_all_model.Rdata")
r <- ggplot(table_model_all, aes(x=cancer_type, y=average_RMSE))
r <- r + geom_jitter(aes(color = tipo))

	#lindo cuando hay pocos modelos, pero tenemos muchos
	#rValues <- resamples(models_sel)
	#bwplot(rValues,metric="RMSE")	

	#había que utilizar la misma aprticiónd e lso datos para genes y exones 
	#y eso no es posible pues hay tipos de cáncer donde no estás las mismas samples en genes y exones
	#comparar resultado pareados
	#friedman
	#library("scmamp")
	#names_m <- paste0("model_", 1:length(models_sel))
	#table_m <- data.frame(model = names_m, average_RMSE = sapply(models_sel, FUN=function(X){X$results$RMSE}), stringsAsFactors = FALSE)
	#table_m <- table_m[order(table_m$average_RMSE), "model"]
	#best <- paste0("",table_m[1])
	#table_m <- data.frame(model = factor(rep(names_m, each=30), levels=table_m), replic = factor(rep(paste0("replic_", 1:30), times=length(models_sel))), RMSE = unlist(lapply(models_sel, FUN=function(X){X$resample$RMSE})))
	#table_m <- reshape(table_m, v.names = "RMSE", idvar = c("replic"), timevar = "model", direction = "wide")
	#result <- postHocTest(data=can[,-1], test="friedman", control=best, correct="holm")
	#rValues <- resamples(models_sel)
	#bwplot(rValues,metric="RMSE")	
}
