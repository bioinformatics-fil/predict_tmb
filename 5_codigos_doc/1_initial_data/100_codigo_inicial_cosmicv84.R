#en las mutaciones dividir el campo Mutation.genomic.position
whole_cosmic_v84_grch38 <- read.delim("../CosmicGenomeScreensMutantExport.tsv")
save(whole_cosmic_v84_grch38, file="0_whole_cosmic.Rdata")
#hay 25533 ID_sample
#eliminar de whole_cosmic donde Mutation.genomic.position sea NA ó ""
whole_cosmic_v84_grch38 <- whole_cosmic_v84_grch38[!(is.na(whole_cosmic_v84_grch38$Mutation.genome.position)),]
whole_cosmic_v84_grch38 <- whole_cosmic_v84_grch38[!(whole_cosmic_v84_grch38$Mutation.genome.position %in% ""),]
#quedan 25499 ID_samples
save(whole_cosmic_v84_grch38, file="1_whole_cosmic.Rdata")

#guardar las coordenadas en un fichero para trabajar más facil
mutations_coordenate <- as.character(unique(whole_cosmic_v84_grch38$Mutation.genome.position))
#verificar que este campo cumpla con las especificación (\\d+)\\:(\\d+)\\-(\\d+)
patron <- "(\\d+)\\:(\\d+)\\-(\\d+)"
x_no_cumple <- grep(x=mutations_coordenate, pattern=patron, invert=TRUE, value=TRUE)
#todos cumplen el patrón, hay 2897374 coordenadas  

#agregarle los campos de forma independiente
h <- as.data.frame(matrix(data= unlist(strsplit(x=mutations_coordenate, split="(\\:|\\-)", perl=TRUE, fixed=FALSE)), ncol=3, byrow=TRUE))
colnames(h) <- c("cromosome", "start", "end")
h$cromosome <- as.character(h$cromosome)
h$start <- as.numeric(as.character(h$start))
h$end <- as.numeric(as.character(h$end))
h <- cbind(Mutation.genome.position=mutations_coordenate, h)
whole_cosmic_v84_grch38 <- merge(whole_cosmic_v84_grch38, h)
save(whole_cosmic_v84_grch38, file="2_whole_cosmic.Rdata")

#dejar las mutaciones que en Mutation.somatic.status son consideradas somáticas c("Confirmed somatic variant", "Reported in another cancer sample as somatic") eliminar los que dicen "Variant of unknown origin",  "Not specified"
whole_cosmic_v84_grch38 <- whole_cosmic_v84_grch38[whole_cosmic_v84_grch38$Mutation.somatic.status %in% c("Confirmed somatic variant", "Reported in another cancer sample as somatic"), ] 
save(whole_cosmic_v84_grch38, file="3_whole_cosmic.RData")
#quedan 25343 ID_sample y 2397875 coordenadas genomicas

#asignar los tipos de cancer 
cancer_types <- read.delim("cancer_type_asignacion.csv")
whole_cosmic_v84_grch38 <- merge(whole_cosmic_v84_grch38, cancer_types, sort=FALSE)
save(whole_cosmic_v84_grch38, file="4_whole_cosmic_cancer_type.RData")
#quedan 24759 ID_sample y 2388197 coordenadas genomicas(unique star, end, cromosoma)
#hay 35 tipos de cáncer


mutations_coordenate <- unique(whole_cosmic_v84_grch38[, c("cromosome", "start", "end")])
mutations_coordenate$cromosome <- as.character(mutations_coordenate$cromosome)
mutations_coordenate[mutations_coordenate$cromosome %in% "23", "cromosome"] <- "X"
mutations_coordenate[mutations_coordenate$cromosome %in% "24", "cromosome"] <- "Y"
mutations_coordenate[mutations_coordenate$cromosome %in% "25", "cromosome"] <- "MT"
mutations_coordenate$start <- as.integer(mutations_coordenate$start)
mutations_coordenate$end <- as.integer(mutations_coordenate$end)
save(mutations_coordenate, file="4_mutation_coordenates.Rdata")

