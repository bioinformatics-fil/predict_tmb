library(ggplot2)
load("56_consensus_filtered_length.Rdata")
select_cancer <- c("non-small cell lung carcinoma", "breast carcinoma", "colorectal carcinoma", "melanoma")
data_selected <- data_selected[data_selected$cancer_type %in% select_cancer, ]
#poner our en strategy porque está escrito oursin
data_selected[data_selected$strategy %in% "oursin - gene", "strategy"] <- "our - gene"
data_selected[data_selected$strategy %in% "oursin - exon", "strategy"] <- "our - exon"
data_selected[data_selected$strategy %in% "census - gene", "strategy"] <- "CGC - gene"
data_selected[data_selected$strategy %in% "census - exon", "strategy"] <- "CGC - exon"
data_selected[data_selected$strategy %in% "panel - gene", "strategy"] <- "FO-panel - gene"
data_selected$length_pb <- data_selected$length_pb /1000000
#R2 vs longitud
p <- ggplot(data_selected, aes(length_pb, R2))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + scale_x_continuous("Number of Mb", trans="log",  limits=c(0.08, 1.815), breaks=c(0.10, 0.25, 0.50, 1.00, 1.50))
p <- p + scale_y_continuous("R2 model", limits=c(0.96, 1.00), breaks=c(0.96, 0.98, 1.00))
p <- p + facet_wrap(~cancer_type, ncol=4)
p <- p + geom_vline(aes(xintercept=0.917923), colour="red")
ggsave("75_R2_vs_lengthpb_consensus_select_cancertype.png", device="png", plot=p, scale = 1, dpi = 300, width=9, height=1.5, units="in" )

#Gráfico de RSE modelo y longitud de pares de bases a secuenciar
load("56_consensus_filtered_length.Rdata")
select_cancer <- c("non-small cell lung carcinoma", "breast carcinoma", "colorectal carcinoma", "melanoma")
library(ggplot2)
library(scales)
data_selected <- data_selected[data_selected$cancer_type %in% select_cancer, ]
#poner our en strategy porque está escrito oursin
data_selected[data_selected$strategy %in% "oursin - gene", "strategy"] <- "our - gene"
data_selected[data_selected$strategy %in% "oursin - exon", "strategy"] <- "our - exon"
data_selected[data_selected$strategy %in% "census - gene", "strategy"] <- "CGC - gene"
data_selected[data_selected$strategy %in% "census - exon", "strategy"] <- "CGC - exon"
data_selected[data_selected$strategy %in% "panel - gene", "strategy"] <- "FO-panel - gene"
data_selected$length_pb <- data_selected$length_pb /1000000

p <- ggplot(data_selected, aes(length_pb, RSE))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + scale_x_continuous("Number of Mb", trans="log",  limits=c(0.08, 1.815), breaks=c(0.10, 0.25, 0.50, 1.00, 1.50))
p <- p + scale_y_continuous("RSE model", limits=c(19, 160), breaks=c(20, 100, 160))
p <- p + facet_wrap(~cancer_type, ncol=4)
p <- p + geom_vline(aes(xintercept=0.917923), colour="red") #cantidad de pares de bases del panel
ggsave("75_RSE_vs_lengthpb_consensus_select_cancertype.png", device="png", plot=p, scale = 1, dpi = 300, width=9, height=1.5, units="in" )


#grafcio de R2 y RSE vs longitud
data_best <- data_selected[, c("cancer_type", "R2", "length_pb", "strategy")]
data_best$metric <- "R2"
colnames(data_best)[2] <- "value"
data_selected <- data_selected[, c("cancer_type", "RSE", "length_pb", "strategy")]
data_selected$metric <- "RSE"
colnames(data_selected)[2] <- "value"
data_best <- rbind(data_best, data_selected)

p <- ggplot(data_best, aes(x=length_pb, y=value))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + labs(y="")
p <- p + scale_x_continuous("Number of Mb", trans="log",  limits=c(0.08, 1.815), breaks=c(0.10, 0.25, 0.50, 1.00, 1.50))
p <- p + facet_grid(metric ~ cancer_type, scale="free_y")
p <- p + geom_vline(aes(xintercept=0.917923), colour="red") #cantidad de pares de bases del panel
ggsave("75_metrics_vs_lengthpb_consensus_select_cancertype.png", device="png", plot=p, scale = 1, dpi = 300, width=9.5, height=2.5, units="in" )

