load("40_subset_exon_sin.Rdata")
count_mut_sample <- unique(subset_select[, c("cancer_type", "total_samples")])

x2 <- unique(subset_select[, c("cancer_type", "ID_sample")])
count_mut_sample2 = aggregate(x = x2$ID_sample, by=list(cancer_type = x2$cancer_type), function(x){length(x)})
colnames(count_mut_sample2)[2] <- "total_samples_clean"
count_mut_sample2 <- merge(count_mut_sample, count_mut_sample2)
count_mut_sample2$percent <- count_mut_sample2$total_samples_clean/ count_mut_sample2$total_samples
write.table(count_mut_sample2, file="40_samples_per_filterexon_sin.csv", quote=FALSE, row.names=FALSE)

y <- unique(subset_select[, c("cancer_type", "ID_sample")])
y$class <- "KEEP"
#utilizar los datos total, antes del filtro de genes para poder comprar los %de samples al aplicar el filtro de genes ó filtro de genes + filtro de exones
load("10_whole_cosmic_fix_new.Rdata")
x3  <- unique(whole_cosmic_fixed[, c("cancer_type", "ID_coordenada", "ID_sample")])
#elimino lo que no necesito por la memoria
rm(whole_cosmic_fixed)
gc()
count_mut_sample3 <- aggregate(x =x3$ID_coordenada, by=list(cancer_type = x3$cancer_type, ID_sample=x3$ID_sample), function(x){length(x)})
colnames(count_mut_sample3)[3] <- "n_mutation"

y <- merge(x=count_mut_sample3, y, all.x=TRUE)
y[is.na(y$class), "class"] <- "DELETE" 

#ordenar por el valor de la mediana de las mutaciones
u <- apply(matrix(as.character(unique(y$cancer_type))), 1, FUN=function(X){h <- y[y$cancer_type %in% X, ]; median(h$n_mutation)})
u <- data.frame(cancer_type = as.character(unique(y$cancer_type)), median=u)
count_mut_sample2 <- merge(count_mut_sample2, u)
count_mut_sample2$cancer_type <- reorder(count_mut_sample2$cancer_type, count_mut_sample2$median)

y <- merge(y, count_mut_sample2) 
y$cancer_type <- reorder(y$cancer_type, y$median)
y$percent <- paste0(round(y$percent*100), "%")
t <- data.frame(cancer_type = levels(y$cancer_type), position=c(10000, 20000)) 
y <- merge(y,t)

library("ggplot2")
p <- ggplot(y, aes(x=cancer_type, y=n_mutation)) + geom_jitter(aes(colour=class))
p <- p + scale_y_continuous("number of mutations", trans="log", limits=c(1, 25000), breaks=c(1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000))
p <- p + geom_text(aes(label=percent, x=cancer_type, y=position, hjust=0.5, vjust=0), show.legend=FALSE, check_overlap = TRUE, fontface = "bold" )
p <- p + theme(text= element_text(size=12), axis.text.x = element_text(angle = 90, hjust=1)) 
p <- p + labs(x="cancer type")
ggsave("40_select_exon_cancertype_sin.png", device="png", plot=p, scale = 1,  dpi = 300, width =12 , height = 6, units = "in")
#hay X tipos de cáncer que quedan con menos del 80% de las samples y son: 
#quedando 21035 samples.
