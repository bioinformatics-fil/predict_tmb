dev.new(width = 7.5, height = 3.5, unit = "in")


library("cowplot")
load("../../data/2_procesado_datos_best/40_datos_density_mutations.Rdata")


#datos de genes en comun
load("../../data/2_procesado_datos_best/40_gene_comun.Rdata")
#media por sample
u <- unique(sample_gene[, c(1:3)])
u$sample <- as.character(u$sample)
u2 <- aggregate(u$gene_comun, by=list(cancer_type=u$cancer_type, sample=u$sample), FUN=median)
colnames(u2)[3] <- "median_gene_by_sample"
#media entre las samples
y1 <- aggregate(u2$median_gene_by_sample, by=list(cancer_type = u2$cancer_type), FUN=median)
colnames(y1)[2] <- "median_cancer_by_sample"

count_mutation_per_sample <- merge(count_mutation_per_sample, y1)
#redondear datos
count_mutation_per_sample$median <- round(count_mutation_per_sample$median, digits = 0)
count_mutation_per_sample$median_cancer_by_sample <- round(count_mutation_per_sample$median_cancer_by_sample, digits = 0)

#hacer el gráfico de violin de las mutaciones.
library("ggplot2")
library("ggridges")

p <- ggplot(count_mutation_per_sample, aes(x = number_mutations, y = cancer_type, group=cancer_type))
p <- p + geom_density_ridges(jittered_points = FALSE, position = "raincloud", alpha = .4, scale=1, fill="#0000ff", size=0.3)
p <- p + scale_x_continuous(name="Number of mutations", limits=c(1, 120000), trans="log", breaks=c(1, 2, 5, 10, 20, 50, 100, 200, 500,1000, 2000, 5000,10000, 20000), expand = c(0, 0))
p <- p + scale_y_discrete(name="Cancer type", expand = c(0.02, 0))
p <- p + geom_text(aes(label=total_samples, y=cancer_type, x=12000, hjust=1, vjust=-0.2), size=1.5, colour="#000000", show.legend=FALSE, check_overlap = TRUE )
p <- p + geom_text(aes(label=median, y=cancer_type, x=40000, hjust=1, vjust=-0.2), size=1.5, colour="#FF0000", show.legend=FALSE, check_overlap = TRUE )
p <- p + geom_text(aes(label=median_cancer_by_sample, y=cancer_type, x=120000, hjust=1, vjust=-0.2), size=1.5, colour="#0000FF", show.legend=FALSE, check_overlap = TRUE )
p <- p + theme(axis.text = element_text(colour = "black"), axis.text.x = element_text(angle = 90, hjust=1, vjust=0.5), axis.text.y = element_text( hjust=1, vjust=0), text= element_text(size=6), axis.title=element_text(size=6), axis.ticks.y=element_blank(), axis.ticks.length.y = unit(0, "cm"))

load("../../data/2_procesado_datos_best/40_subset_sin.Rdata")
 
count_mut_sample <- unique(subset_select[, c("cancer_type", "total_samples")])
x2 <- unique(subset_select[, c("cancer_type", "ID_sample")])
count_mut_sample2 = aggregate(x = x2$ID_sample, by=list(cancer_type = x2$cancer_type), function(x){length(x)})
colnames(count_mut_sample2)[2] <- "total_samples_clean"
count_mut_sample2 <- merge(count_mut_sample, count_mut_sample2)
count_mut_sample2$percent <- count_mut_sample2$total_samples_clean/ count_mut_sample2$total_samples

load("../../data/1_inicial_datos_best/10_whole_cosmic_fix_new.Rdata")
y <- unique(subset_select[, c("cancer_type", "ID_sample")])
y$class <- "KEEP"
x3 <- unique(whole_cosmic_fixed[, c("cancer_type", "ID_coordenada", "ID_sample")])
#elimino lo que no necesito por la memoria
rm(whole_cosmic_fixed)
gc()

count_mut_sample3 <- aggregate(x =x3$ID_coordenada, by=list(cancer_type = x3$cancer_type, ID_sample=x3$ID_sample), function(x){length(x)})
colnames(count_mut_sample3)[3] <- "n_mutation"
y <- merge(x=count_mut_sample3, y, all.x=TRUE)
y[is.na(y$class), "class"] <- "DELETE" 

#ordenar por el valor de la mediana las mutaciones
u <- apply(matrix(as.character(unique(y$cancer_type))), 1, FUN=function(X){h <- y[y$cancer_type %in% X, ]; median(h$n_mutation)})
u <- data.frame(cancer_type = as.character(unique(y$cancer_type)), median=u)
count_mut_sample2 <- merge(count_mut_sample2, u)
count_mut_sample2$cancer_type <- reorder(count_mut_sample2$cancer_type, count_mut_sample2$median)

y <- merge(y, count_mut_sample2) 
y$cancer_type <- reorder(y$cancer_type, y$median)
y$percent <- paste0(round(y$percent*100), "%")
t <- data.frame(cancer_type = levels(y$cancer_type), position=c(10000, 20000)) 
y <- merge(y,t)

library("ggplot2")
q <- ggplot(y, aes(x=cancer_type, y=n_mutation)) 
q <- q + geom_jitter(aes(colour=class), size=0.2)
q <- q + scale_y_continuous("Number of mutations", trans="log", limits=c(1, 25000), breaks=c(1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000))
q <- q + geom_text(aes(label=percent, y=position, x=cancer_type, hjust=0.5, vjust=0), size=1.5, show.legend=FALSE, check_overlap = TRUE, fontface = "bold" )
q <- q + theme(axis.text = element_text(colour = "black"), axis.text.x = element_text(angle = 90, hjust=1, vjust=0.5), text= element_text(size=6), legend.position = "bottom", axis.title=element_text(size=6)) 
q <- q + labs(x="Cancer type")
q <- q + guides(color = guide_legend(override.aes = list(size = 1.5, alpha=1)))



pq <- plot_grid(p, q, labels = "auto",  rel_widths=c(0.35, 0.65), label_size=10)
pq

ggsave("Fig1_paper.png", device="png", plot=pq, scale = 1,  dpi = 600, width=7.5 , height = 3.5, units = "in")
ggsave("Fig1_paper.svg", device="svg", plot=pq, scale = 1,  dpi = 600, width=7.5 , height = 3.5, units = "in")


