#gráfico de puntos de los % samples con al menos una mutacion del external test. vs training
load("56_consensus_filtered_length.Rdata")
library("ggplot2")
#poner our en strategy porque está escrito oursin
data_selected[data_selected$strategy %in% "oursin - gene", "strategy"] <- "our - gene"
data_selected[data_selected$strategy %in% "oursin - exon", "strategy"] <- "our - exon"
data_selected[data_selected$strategy %in% "census - gene", "strategy"] <- "CGC - gene"
data_selected[data_selected$strategy %in% "census - exon", "strategy"] <- "CGC - exon"
data_selected[data_selected$strategy %in% "panel - gene", "strategy"] <- "FO-panel - gene"


p <- ggplot(data_selected, aes(percent_training, percent_testing))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + scale_y_continuous("% samples in testing", limits=c(25, 100), breaks=c(30, 40, 50, 60, 70, 80, 90, 100))
p <- p + scale_x_continuous("% samples in training", limits=c(25, 100), breaks=c(30, 40, 50, 60, 70, 80, 90, 100))
p <- p + geom_hline(aes(yintercept=50), colour="red")
p <- p + geom_vline(aes(xintercept=50), colour="red")
p <- p + facet_wrap(~cancer_type, ncol=5)
ggsave("71_percentsamples_train_vs_test_consensus.png", device="png", plot=p, scale = 1, dpi = 300, width=11, height=6, units="in" )


#grafico R2 internal vs R2 modelo
load("56_consensus_filtered_length.Rdata")
library("ggplot2")
#poner our en strategy porque está escrito oursin
data_selected[data_selected$strategy %in% "oursin - gene", "strategy"] <- "our - gene"
data_selected[data_selected$strategy %in% "oursin - exon", "strategy"] <- "our - exon"
data_selected[data_selected$strategy %in% "census - gene", "strategy"] <- "CGC - gene"
data_selected[data_selected$strategy %in% "census - exon", "strategy"] <- "CGC - exon"
data_selected[data_selected$strategy %in% "panel - gene", "strategy"] <- "FO-panel - gene"
p <- ggplot(data_selected, aes(Rsquared_1000optboot, R2))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + scale_x_continuous("R2 internal validation", limits=c(0.6, 1.00), breaks=c(0.6, 0.70, 0.80, 0.90, 1.00))
p <- p + scale_y_continuous("R2 model", limits=c(0.60, 1.00), breaks=c(0.6, 0.8, 1.00))
p <- p + facet_wrap(~cancer_type, ncol=5)
ggsave("71_R2_train_vs_model_consensus.png", device="png", plot=p, scale = 1, dpi = 300, width=11, height=4.5, units="in" )


#grafico RSE interno vs RSE modelo
load("56_consensus_filtered_length.Rdata")
library("ggplot2")
#poner our en strategy porque está escrito oursin
data_selected[data_selected$strategy %in% "oursin - gene", "strategy"] <- "our - gene"
data_selected[data_selected$strategy %in% "oursin - exon", "strategy"] <- "our - exon"
data_selected[data_selected$strategy %in% "census - gene", "strategy"] <- "CGC - gene"
data_selected[data_selected$strategy %in% "census - exon", "strategy"] <- "CGC - exon"
data_selected[data_selected$strategy %in% "panel - gene", "strategy"] <- "FO-panel - gene"
p <- ggplot(data_selected, aes(RMSE_1000optboot, RSE))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + scale_x_continuous("RSE internal validation", trans="log", limits=c(16, 250), breaks=c(30, 50, 100, 150, 200))
p <- p + scale_y_continuous("RSE model", trans="log", limits=c(16, 230), breaks=c(50, 100, 200))
p <- p + facet_wrap(~cancer_type, ncol=5)
ggsave("71_RSE_train_vs_model_consensus.png", device="png", plot=p, scale = 1, dpi = 300, width=11, height=4.5, units="in")


#grafico R2 external vs R2 modelo
load("56_consensus_filtered_length.Rdata")
library("ggplot2")
#poner our en strategy porque está escrito oursin
data_selected[data_selected$strategy %in% "oursin - gene", "strategy"] <- "our - gene"
data_selected[data_selected$strategy %in% "oursin - exon", "strategy"] <- "our - exon"
data_selected[data_selected$strategy %in% "census - gene", "strategy"] <- "CGC - gene"
data_selected[data_selected$strategy %in% "census - exon", "strategy"] <- "CGC - exon"
data_selected[data_selected$strategy %in% "panel - gene", "strategy"] <- "FO-panel - gene"
p <- ggplot(data_selected, aes(Rsquared_externaltest, R2))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + scale_x_continuous("R2 external validation", limits=c(0.6, 1.00), breaks=c(0.60, 0.7, 0.8 , 0.9,  1.00))
p <- p + scale_y_continuous("R2 model", limits=c(0.6, 1.00), breaks=c(0.6, 0.8, 1.00))
p <- p + facet_wrap(~cancer_type, ncol=5)
ggsave("71_R2_train_vs_test_consensus.png", device="png", plot=p, scale = 1, dpi = 300, width=11, height=4.5, units="in" )


#grafico RSE externo vs RSE modelo
load("56_consensus_filtered_length.Rdata")
library("ggplot2")
#poner our en strategy porque está escrito oursin
data_selected[data_selected$strategy %in% "oursin - gene", "strategy"] <- "our - gene"
data_selected[data_selected$strategy %in% "oursin - exon", "strategy"] <- "our - exon"
data_selected[data_selected$strategy %in% "census - gene", "strategy"] <- "CGC - gene"
data_selected[data_selected$strategy %in% "census - exon", "strategy"] <- "CGC - exon"
data_selected[data_selected$strategy %in% "panel - gene", "strategy"] <- "FO-panel - gene"
p <- ggplot(data_selected, aes(RMSE_externaltest, RSE))
p <- p + geom_point(shape=16, size=1, position= position_jitter(), alpha=0.5, aes(color=strategy))
p <- p + theme_bw() + theme(axis.text=element_text(size=12), axis.text.x = element_text(hjust=1, vjust=0.5, angle=90))
p <- p + scale_x_continuous("RSE external validation", trans="log", limits=c(16, 1543), breaks=c(30, 100, 200, 500, 1000))
p <- p + scale_y_continuous("RSE model", trans="log", limits=c(16, 230), breaks=c(50, 100, 200))
p <- p + facet_wrap(~cancer_type, ncol=5)
ggsave("71_RSE_train_vs_test_consensus.png", device="png", plot=p, scale = 1, dpi = 300, width=11, height=4.5, units="in")



