load("42_stat_plot_gene_sin.Rdata")
load("41_subset_all_data_gene_sin.Rdata")
whole_cosmic_select$gene_hgnc <- paste0(whole_cosmic_select$hgnc_symbol,"-", whole_cosmic_select$gene_ensembl)

gene_hgnc <- unique(whole_cosmic_select[, c("gene_ensembl", "hgnc_symbol", "gene_hgnc")])
colnames(gene_hgnc)[1] <- "gene_add"

genes_10_first <- stat_plot_gene[stat_plot_gene$n_genes <= 10, c("cancer_type","gene_add", "acum_percent", "samples_importancia", "percent" )]
genes_10_first <- merge(x=genes_10_first, y=gene_hgnc)

total_genes <- length(unique(genes_10_first$gene_hgnc))

genes_10_first <- rbind((genes_10_first[, c("cancer_type", "gene_hgnc", "percent")]), data.frame(cancer_type=unique(genes_10_first$cancer_type), gene_hgnc = "first_10", percent = apply(matrix(unique(genes_10_first$cancer_type)), 1, FUN=function(X){max(genes_10_first[genes_10_first$cancer_type == X, "acum_percent"])})))

x <- genes_10_first[genes_10_first$gene_hgnc == "first_10", c("cancer_type", "percent")]
x <- x[order(-x$percent),]
genes_10_first$cancer_type <- factor(genes_10_first$cancer_type, levels=as.character(x$cancer_type))


x <- unique(merge(whole_cosmic_select, genes_10_first)[, c("gene_hgnc", "cancer_type", "importancia")])  
x <- aggregate(x$importancia, by=list(gene_hgnc = x$gene_hgnc), FUN=sum)
x <- x[order(-x$x),]
genes_10_first$gene_hgnc <- factor(genes_10_first$gene_hgnc, levels=c("first_10", as.character(x$gene_hgnc)))

genes_10_first$percentFactor <- cut(genes_10_first$percent, breaks = c(0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1), labels=c("0-0.1", "0.1-0.2", "0.2-0.3", "0.3-0.4", "0.4-0.5", "0.5-0.6", "0.6-0.7", "0.7-0.8", "0.8-0.9", "0.9-1"))

#ver cuantos genes salen
library("ggplot2")
r <- ggplot(genes_10_first[genes_10_first$gene_hgnc %in% levels(genes_10_first$gene_hgnc )[1:21],], aes(x=cancer_type, y=gene_hgnc)) 
r <- r + geom_tile(aes(fill = percentFactor), color = "gray") 
r <- r + labs(title = paste0("First 10 genes per cancer type (20 first / ",total_genes," genes)"), x = "Cancer type", y = "Gene name (HUGO - Ensembl gene)")
r <- r + theme(plot.title = element_text(size=10), axis.text.x = element_text(angle = 90, hjust = 1, vjust=0.05)) 
r <- r + scale_fill_manual(values = c("#ffffd9", "#fee8c8", "#fdd49e", "#fdbb84", "#fc8d59", "#ef6548", "#d7301f", "#b30000", "#7f0000", "#000000"))

ggsave("43_First_10_genes_sin.png", device="png", plot=r, scale = 1,  dpi = 300, width=7.1, height=5, units="in")
