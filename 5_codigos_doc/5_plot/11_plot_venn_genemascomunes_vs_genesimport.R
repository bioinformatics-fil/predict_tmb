#genes mas frecuentemente mutados por cancer type, quedarnos con los 10 primeros
#similar a la parte inicial del fichero 04_xxx
load("10_whole_cosmic_fix_new.Rdata")
data <- unique(whole_cosmic_fixed[, c("ID_sample", "gene_ensembl", "hgnc_symbol", "cancer_type")])
data$gene_hgnc <- paste0(data$hgnc_symbol,"-", data$gene_ensembl)
data <- data[c("ID_sample", "gene_hgnc", "cancer_type")]
rm(whole_cosmic_fixed)

c <- unique(data[, c("ID_sample", "cancer_type")])
c <- aggregate(c$ID_sample, by=list(cancer_type = c$cancer_type), FUN=length)
colnames(c)[2] <- "total_samples"

s <- aggregate(data$ID_sample, by=list(cancer_type = data$cancer_type, gene_hgnc=data$gene_hgnc), FUN=length)
colnames(s)[3] <- "samples"
s <- merge(s, c)
s$percent <- s$sample / s$total_samples

y <- unique(data$cancer_type)
y <- lapply(y, FUN=function(X){
	t <- s[s$cancer_type %in% X, ]
	t <- t[order(-t$samples), ]
	t[c(1:min(length(t[,1]),10)), c("cancer_type", "gene_hgnc")]
})
y <- Reduce(rbind, y)
s <- merge(s, y)

genes_most_mutated <- unique(s$gene_hgnc)

# los 10 primeros genes mas importantes
#inicial del file 11_plot_10genes
load("42_stat_plot_gene_sin.Rdata")
load("41_subset_all_data_gene_sin.Rdata")
whole_cosmic_select$gene_hgnc <- paste0(whole_cosmic_select$hgnc_symbol,"-", whole_cosmic_select$gene_ensembl)

gene_hgnc <- unique(whole_cosmic_select[, c("gene_ensembl", "hgnc_symbol", "gene_hgnc")])
colnames(gene_hgnc)[1] <- "gene_add"

genes_10_first <- stat_plot_gene[stat_plot_gene$n_genes <= 10, c("cancer_type","gene_add", "acum_percent", "samples_importancia", "percent" )]
genes_10_first <- merge(x=genes_10_first, y=gene_hgnc)

genes_important <- unique(genes_10_first$gene_hgnc)

library(VennDiagram)

venn.diagram(list(frequent=genes_most_mutated, important=genes_important), filename="43_venns_genes_freq_import.png", 
height = 600, width = 600, resolution = 300, imagetype = "png", units = "px", lty = "blank", 
fill = c("skyblue", "pink1"),  rotation = 1, reverse = FALSE, euler.d = TRUE, scaled = TRUE, 
lwd = rep(2, 2), col = rep("black", 2), alpha = rep(0.5, 2), label.col = rep("black", 3), cex = rep(0.75, 3), 
fontface = rep("plain", 3), fontfamily = rep("serif", 3),  category.names = c("Frequent", "Important"), 
cat.pos = c(-20, 20), cat.dist = c(0.05, 0.05), cat.col = rep("black", 2), cat.cex = rep(1, 2), 
cat.fontface = rep("plain", 2), cat.fontfamily = rep("serif", 2), cat.just = list(c(0.5, 1), c(0.5, 1)), 
cat.default.pos = "outer", cat.prompts = FALSE, rotation.degree = 0, rotation.centre = c(0.5, 0.5), 
sep.dist = 0.05, offset = 0, cex.prop = NULL, print.mode = "raw", sigdigs = 3, direct.area = FALSE, area.vector = 0)

