library("stringr")

#Calculamos wilcoxon a los modelos filtrados, para decidir que son mejores los modelos con genes que los de exones
load("56_consensus_filtered_length.Rdata")

wilcox.test(data_selected[data_selected$tipo %in% "gene", "RSE"], data_selected[data_selected$tipo %in% "exon", "RSE"], alternative = "two")$p.value
wilcox.test(data_selected[data_selected$tipo %in% "gene", "RSE"], data_selected[data_selected$tipo %in% "exon", "RSE"], alternative = "less")$p.value

wilcox.test(data_selected[data_selected$tipo %in% "gene", "RMSE_1000optboot"], data_selected[data_selected$tipo %in% "exon", "RMSE_1000optboot"], alternative = "two")$p.value
wilcox.test(data_selected[data_selected$tipo %in% "gene", "RMSE_1000optboot"], data_selected[data_selected$tipo %in% "exon", "RMSE_1000optboot"], alternative = "less")$p.value

wilcox.test(data_selected[data_selected$tipo %in% "gene", "RMSE_externaltest"], data_selected[data_selected$tipo %in% "exon", "RMSE_externaltest"], alternative = "two")$p.value
wilcox.test(data_selected[data_selected$tipo %in% "gene", "RMSE_externaltest"], data_selected[data_selected$tipo %in% "exon", "RMSE_externaltest"], alternative = "less")$p.value

wilcox.test(data_selected[data_selected$tipo %in% "gene", "R2"], data_selected[data_selected$tipo %in% "exon", "R2"], alternative = "two")$p.value 
wilcox.test(data_selected[data_selected$tipo %in% "gene", "R2"], data_selected[data_selected$tipo %in% "exon", "R2"], alternative = "greater")$p.value

wilcox.test(data_selected[data_selected$tipo %in% "gene", "Rsquared_1000optboot"], data_selected[data_selected$tipo %in% "exon", "Rsquared_1000optboot"], alternative = "two")$p.value
wilcox.test(data_selected[data_selected$tipo %in% "gene", "Rsquared_1000optboot"], data_selected[data_selected$tipo %in% "exon", "Rsquared_1000optboot"], alternative = "greater")$p.value

wilcox.test(data_selected[data_selected$tipo %in% "gene", "Rsquared_externaltest"], data_selected[data_selected$tipo %in% "exon", "Rsquared_externaltest"], alternative = "two")$p.value
wilcox.test(data_selected[data_selected$tipo %in% "gene", "Rsquared_externaltest"], data_selected[data_selected$tipo %in% "exon", "Rsquared_externaltest"], alternative = "greater")$p.value

#Calculamos kruskal wallis y post hoc dunn test  para decidir que nuestros modelos son mejores que los de panel y de census
load("56_consensus_filtered_length.Rdata")
data_selected$strategy <- factor(data_selected$strategy, levels=c("oursin - gene", "oursin - exon", "census - gene", "census - exon", "panel - gene", "panel - exon"))

kruskal.test(RSE ~ strategy, data=data_selected)$p.value
kruskal.test(RMSE_1000optboot ~ strategy, data=data_selected)$p.value
kruskal.test(RMSE_externaltest ~ strategy, data=data_selected)$p.value
kruskal.test(R2 ~ strategy, data=data_selected)$p.value
kruskal.test(Rsquared_1000optboot ~ strategy, data=data_selected)$p.value
kruskal.test(Rsquared_externaltest ~ strategy, data=data_selected)$p.value

#todos son differentes. Hacer comparacion post hoc en cada variable utilizando our selection como control.
library("FSA")
library("PMCMRplus")

t <- Summarize(RSE ~ strategy, data=data_selected)
t <- rbind(t, Summarize(RMSE_1000optboot~ strategy, data=data_selected))
t <- rbind(t, Summarize(RMSE_externaltest ~ strategy, data=data_selected)[colnames(t)])
t <- rbind(t, Summarize(R2 ~ strategy, data=data_selected))
t <- rbind(t, Summarize(Rsquared_1000optboot~ strategy, data=data_selected))
t <- rbind(t, Summarize(Rsquared_externaltest ~ strategy, data=data_selected)[colnames(t)])
write.table(t, file="59_zz.txt", row.names=FALSE, quote=FALSE, sep="\t")

t <- t(data.frame(kwManyOneDunnTest(x=data_selected$R2 ,g=data_selected$strategy, p.adjust="fdr")$p.value))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected$Rsquared_1000optboot ,g=data_selected$strategy, p.adjust="fdr")$p.value)))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected$Rsquared_externaltest ,g=data_selected$strategy, p.adjust="fdr")$p.value)))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected$RSE ,g=data_selected$strategy, p.adjust="fdr")$p.value)))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected$RMSE_1000optboot ,g=data_selected$strategy, p.adjust="fdr")$p.value)))
 
t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected$RMSE_externaltest ,g=data_selected$strategy, p.adjust="fdr")$p.value)))
write.table(t, file="59_zz2.txt", row.names=FALSE, quote=FALSE, sep="\t")

#Calculamos kruskal wallis y post hoc dunn test  para decidir que nuestros modelos son mejores que los de panel y de census en los cancer types donde aparecen varias strategias
load("56_consensus_filtered_length.Rdata")
library("FSA")
library("PMCMRplus")

cancer <- c("glioma", "colorectal carcinoma", "liver carcinoma", "ovarian serous carcinoma", "non-melanoma skin tumor", "melanoma", "gastic carcinoma", "uninary tract other")
data_selected$strategy <- factor(data_selected$strategy, levels=c("oursin - gene", "oursin - exon", "census - gene", "census - exon", "panel - gene", "panel - exon"))
for(i in cancer){
print(i)
print(kruskal.test(RSE ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])$p.value)
print(kruskal.test(RMSE_1000optboot ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])$p.value)
print(kruskal.test(RMSE_externaltest ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])$p.value)
print(kruskal.test(R2 ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])$p.value)
print(kruskal.test(Rsquared_1000optboot ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])$p.value)
print(kruskal.test(Rsquared_externaltest ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])$p.value)

#todos son differentes. Hacer comparacion post hoc en cada variable utilizando our selection como control.

t <- Summarize(RSE ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])
t <- rbind(t, Summarize(RMSE_1000optboot~ strategy, data=data_selected[data_selected$cancer_type %in% i,]))
t <- rbind(t, Summarize(RMSE_externaltest ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])[colnames(t)])
t <- rbind(t, Summarize(R2 ~ strategy, data=data_selected[data_selected$cancer_type %in% i,]))
t <- rbind(t, Summarize(Rsquared_1000optboot~ strategy, data=data_selected[data_selected$cancer_type %in% i,]))
t <- rbind(t, Summarize(Rsquared_externaltest ~ strategy, data=data_selected[data_selected$cancer_type %in% i,])[colnames(t)])
write.table(t, file=paste0("59_zz_",str_replace(i, " ", "_"),".txt"), row.names=FALSE, quote=FALSE, sep="\t")

t <- t(data.frame(kwManyOneDunnTest(x=data_selected[data_selected$cancer_type %in% i,"R2"] ,g=data_selected[data_selected$cancer_type %in% i,"strategy"] , p.adjust="bonferroni")$p.value))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected[data_selected$cancer_type %in% i,"Rsquared_1000optboot"]  ,g=data_selected[data_selected$cancer_type %in% i,"strategy"] , p.adjust="fdr")$p.value)))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected[data_selected$cancer_type %in% i,"Rsquared_externaltest"]  ,g=data_selected[data_selected$cancer_type %in% i,"strategy"] , p.adjust="fdr")$p.value)))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected[data_selected$cancer_type %in% i,"RSE"]  ,g=data_selected[data_selected$cancer_type %in% i,"strategy"] , p.adjust="fdr")$p.value)))

t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected[data_selected$cancer_type %in% i,"RMSE_1000optboot"]  ,g=data_selected[data_selected$cancer_type %in% i,"strategy"] , p.adjust="fdr")$p.value)))
 
t <- rbind(t, t(data.frame(kwManyOneDunnTest(x=data_selected[data_selected$cancer_type %in% i,"RMSE_externaltest"] ,g=data_selected[data_selected$cancer_type %in% i,"strategy"] , p.adjust="fdr")$p.value)))
write.table(t, file=paste0("59_zz2_",str_replace(i, " ", "_"),".txt"), row.names=FALSE, quote=FALSE, sep="\t")
}
