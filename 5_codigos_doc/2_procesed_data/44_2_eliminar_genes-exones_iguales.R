#load("42_stat_plot_gene.Rdata")
#gene_importancia_cancer <- stat_plot_gene[, c("cancer_type", "gene_add", "samples_importancia")]
#load("41_subset_all_data_gene.Rdata")
#gene_sample_cancer <- unique(whole_cosmic_select[, c("ID_sample", "gene_ensembl", "cancer_type")]) 

ver_iguales_genes <- function(gene_sample_cancer, gene_importancia_cancer){
#me quedo con los genes ordenados
gene_importancia_cancer <- gene_importancia_cancer[order(gene_importancia_cancer$cancer_type, -gene_importancia_cancer$samples_importancia),]

#lista de cancer
cancers <- as.list(as.character(unique(gene_sample_cancer$cancer_type)))
k <- lapply(cancers, FUN=function(cancer){
print(cancer)
	x <- gene_sample_cancer[gene_sample_cancer$cancer_type %in% cancer, c("ID_sample", "gene_ensembl")]
	#lista se samples por cada gene
	genes <- as.character(unique(gene_importancia_cancer[gene_importancia_cancer$cancer_type %in% cancer, "gene_add"]))
	y <- lapply(as.list(as.character(genes)), FUN=function(X){
		unique(x[x$gene_ensembl %in% X, "ID_sample"])
	})	
	#ordenar los genes por importancia
	#lista de genes seleccionados
	genes_sel <- c()
	while(length(y) > 1){
		ver_gene <- genes[1]
		samples_gene <- y[[1]]
		y <- y[-1]
		genes <- genes[-1]
		genes_sel <- c(genes_sel, ver_gene)
		z <- sapply(y, FUN=function(X){ setequal(samples_gene, X)})
		if(any(z == TRUE)){
			y <- y[!z]
			genes <- genes[!z]
		}		
	}
	genes_sel <- c(genes_sel, genes[1])
	data.frame(cancer_type = cancer, gene_ensembl = genes_sel, stringsAsFactors=FALSE)
})
j <- Reduce(rbind, k)
}

#load("42_stat_plot_exon.Rdata")
#exon_importancia_cancer <- stat_plot_exon[, c("cancer_type", "exon_add", "importancia")]
#load("41_subset_all_data_exon.Rdata")
#exon_sample_cancer <- unique(whole_cosmic_select[, c("ID_sample", "exon_ensembl", "cancer_type")]) 
ver_iguales_exones <- function(exon_sample_cancer, exon_importancia_cancer){
#me quedo con los genes ordenados
exon_importancia_cancer <- exon_importancia_cancer[order(exon_importancia_cancer$cancer_type, -exon_importancia_cancer$importancia),]

#lista de cancer
cancers <- as.list(as.character(unique(exon_sample_cancer$cancer_type)))
k <- lapply(cancers, FUN=function(cancer){
print(cancer)
	x <- exon_sample_cancer[exon_sample_cancer$cancer_type %in% cancer, c("ID_sample", "exon_ensembl")]
	#lista se samples por cada gene
	exons <- as.character(unique(exon_importancia_cancer[exon_importancia_cancer$cancer_type %in% cancer, "exon_add"]))
	y <- lapply(as.list(as.character(exons)), FUN=function(X){
		unique(x[x$exon_ensembl %in% X, "ID_sample"])
	})	
	#ordenar los genes por importancia
	#lista de genes seleccionados
	exons_sel <- c()
	while(length(y) > 1){
		ver_exon <- exons[1]
		samples_exon <- y[[1]]
		y <- y[-1]
		exons <- exons[-1]
		exons_sel <- c(exons_sel, ver_exon)
		z <- sapply(y, FUN=function(X){ setequal(samples_exon, X)})
		if(any(z == TRUE)){
			y <- y[!z]
			exons <- exons[!z]
		}		
	}
	exons_sel <- c(exons_sel, exons[1])
	data.frame(cancer_type = cancer, exon_ensembl = exons_sel, stringsAsFactors=FALSE)
})
j <- Reduce(rbind, k)

}
