poner bien los cromosomas y agregarle la columna pubmed
lista_archivos <- list.files("1_bien_start_end/")
library(stringr)
new_files <- paste0("m_", lista_archivos)
pubmed <- str_split(string=lista_archivos, pattern="_|\\.")
pubmed <- sapply(pubmed, FUN=function(X){X[1]})
for(i in 1:length(lista_archivos)){
	print(lista_archivos[i])
	y <- read.delim(paste0("1_bien_start_end/", lista_archivos[i]), sep="\t", stringsAsFactors=FALSE)
	#agrego columna pubmed
	y$pubmed <- pubmed[i]
	#dejar los cromosomas como 1:22, X, Y, M
	y$cromosoma <- as.character(y$cromosoma)
	y$cromosoma <- str_replace_all(string=y$cromosoma, pattern="(chr)|(CHR)", replacement="")	
	#M puede venir como MT o 25
	#X puede venir como 23
	#Y puede venir como 24
	y$cromosoma <- str_replace_all(string=y$cromosoma, pattern="T", replacement="") 
	y$cromosoma <- str_replace_all(string=y$cromosoma, pattern="23", replacement="X")
	y$cromosoma <- str_replace_all(string=y$cromosoma, pattern="24", replacement="Y")
	y$cromosoma <- str_replace_all(string=y$cromosoma, pattern="25", replacement="M")
	k <- unique(y$cromosoma)
	print(k[!(k %in% c("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y", "M" ))])
	y <- y[y$cromosoma %in% c("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y", "M" ),]
	write.table(y, file=paste0("2_bien_cromosoma/", new_files[i]), sep="\t", quote=FALSE, row.names=FALSE)
}

