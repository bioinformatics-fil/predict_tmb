#a los ficheron que no había que hacerle nada, verificar los nombres de las columnas y que start y end sean números enteros.
lista_archivos <- list.files("0_datos/datos/")
library(stringr)
for(i in 1:length(lista_archivos)){
	print(lista_archivos[i])
	y <- read.delim(paste0("0_datos/datos/", lista_archivos[i]), sep="\t", stringsAsFactors=FALSE)
	if(any(colnames(y) %in% c("sample", "cromosoma", "start", "end", "ref", "alt", "cancer_type") == FALSE)){
		print(paste0("error en columnas ", lista_archivos[i]))
	}
	if(length(y) != 7){
		print(paste0("length ", length(y)))
	}
}
