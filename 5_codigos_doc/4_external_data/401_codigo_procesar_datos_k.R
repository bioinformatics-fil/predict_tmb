#A los *_k.csv verificar si tienen posiciones consecutivas, si lo tienen juntarlas. Guardarlos en “1_bien_start_end”
lista_archivos <- list.files("0_datos/datos_k/")
library(stringr)
new_files <- str_replace_all(string=lista_archivos, pattern="_k", replacement="")

fix_sep_line <- function(data){
#data: sample, cromosoma, start, end, ref, alt
data <- data[order(data$sample, data$cromosoma, data$start),]
count <- 0
ref2 <- ""
alt2 <- ""
for(i in length(data[,1]):1){
	if(i > 1 && data[i, "sample"] == data[i - 1, "sample"] && data[i, "cromosoma"] == data[i - 1, "cromosoma"] && data[i, "start"] - 1 == data[i - 1, "start"]){
		count <- count + 1	
		ref2 <- paste0(data[i, "ref"], ref2)	
		alt2 <- paste0(data[i, "alt"], alt2)	
		print(i)
	}else{
		if(count > 0){ #elimino algunas samples
			data[i, "ref"] <- paste0(data[i, "ref"], ref2)
			data[i, "alt"] <- paste0(data[i, "alt"], alt2)
			sel <- c(1:i)
			if( (i+count+1) <= length(data[,1])){
				sel <- c(sel, c((i+count+1):length(data[,1])))
			}
			data <- data[sel,]
			data[i, "end"] <- data[i, "start"]  + count
			count <- 0
			ref2 <- ""
			alt2 <- ""
		}
	}	
}
data
}

for(i in 1:length(lista_archivos)){
	print(lista_archivos[i])
	y <- read.delim(paste0("0_datos/datos_k/", lista_archivos[i]), sep="\t", stringsAsFactors=FALSE)
	#if(any(colnames(y) %in% c("sample", "cromosoma", "start", "end", "ref", "alt", "cancer_type") == FALSE)){
	#	print(paste0("nombre mal ", lista_archivos[i]))
	#}
	if(!any(colnames(y) %in% "end" == TRUE)){
		y$end <- y$start
	}
	k <- fix_sep_line(y)
	write.table(k, file=paste0("1_bien_start_end/", new_files[i]), sep="\t", quote=FALSE, row.names=FALSE)
}
